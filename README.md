An SSH tarpit.

Executable is `endlessh.py` (which doesn't collide with `endlessh`).

A systemd unit file is provided that is parameterized by port. To have the tarpit run on port 22 do:

```
systemctl enable endlessh@22
systemctl start endlessh@22
```

## License

GPLv3

## Credits
Based on the Python snippet here: https://nullprogram.com/blog/2019/03/22/

sd-notify support based on code here: https://github.com/bb4242/sdnotify/blob/d349a649d961b02df4755efa2e21807940227c4d/sdnotify/__init__.py
