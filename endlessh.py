#!/usr/bin/env python3

import argparse
import asyncio
import os
import random
import socket

async def handler(_reader, writer):
    try:
        while True:
            await asyncio.sleep(10)
            writer.write(b'%x\r\n' % random.randint(0, 2**32))
            await writer.drain()
    except ConnectionResetError:
        pass

async def server(port):
    server = await asyncio.start_server(handler, '0.0.0.0', port)
    async with server:
        sd_notify_ready()
        await server.serve_forever()

def main():
    parser = argparse.ArgumentParser(description='A tarpit SSH server.')
    parser.add_argument('-p', '--port', type=int, default=22, help='the port to bind to')
    args = parser.parse_args()
    asyncio.run(server(args.port))


def sd_notify_ready():
    sd_notify('READY=1')


def sd_notify(state):
    socket_path = os.environ.get('NOTIFY_SOCKET')
    if socket_path is None:
        return
    socket_path = socket_path.encode('ascii')
    # socket_path starts with @ if it's a Linux "abstract namespace" socket.
    if socket_path[0] == b'@':
        # apparently a null-byte tells connect() it's an abstract namespace.
        socket_path = socket_path[1:]
        socket_path = b'\0' + socket_path
    with socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM) as s:
        s.connect(socket_path)
        s.sendall(state.encode('utf-8'))


if __name__ == '__main__':
    main()
